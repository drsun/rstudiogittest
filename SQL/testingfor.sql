
drop table ts_prostate_ccr_data_2018;
CREATE TABLE ts_prostate_ccr_data_2018 (
TUMOR_ID                    NUMBER(7),
site_02                     VARCHAR2(4),
LATERAL                     NUMBER(1),
histo_t3                    NUMBER(4),
histo_m3                    NUMBER(1),
differn2                    NUMBER(1),
seqnocen                    NUMBER(2),
COUNTY                      NUMBER(2),
censustrcertainty2010       NUMBER(1),
CHEMOSUM                    NUMBER(2),
dxconf                      NUMBER(1),
dx_stg_sum                  NUMBER(1),
hormsum                     NUMBER(2),
immusum                     NUMBER(2),
MARSTAT                     NUMBER(1),
xnodetu                     NUMBER(2),
PNODETU                     NUMBER(2),
radbstmod                   NUMBER(2),
nosurg                      NUMBER(1),
typerep                     NUMBER(1),
rxsummsystemicsurseq        NUMBER(1),
nnodes                      NUMBER(2),
SURGPRIM                    NUMBER(2),
tustat                      NUMBER(1),
YEARDX                      NUMBER(4),
PAYER                       NUMBER(2),
payer2                      NUMBER(2),
surgp1                      NUMBER(2),
surgp2                      NUMBER(2),
surgp3                      NUMBER(2),
mcode_p                     VARCHAR2(3),
ncode_p                     VARCHAR2(3),
ajcc_p                      VARCHAR2(3),
TCODE_P                     VARCHAR2(3),  
datedx                      NUMBER(8),
datestat                    NUMBER(8),
rxdatec                     NUMBER(8),
rxdateh                     NUMBER(8),
rxdatei                     NUMBER(8),
RXDATER                     NUMBER(8),
rxdatesn                    NUMBER(8),
surgdate                    NUMBER(8),
DTDEFSURG                   NUMBER(8),
rxdate                      NUMBER(8),
age                         NUMBER(2),
cs_ext                      VARCHAR2(3),
cs_ln                       NUMBER(3),
cs_mets_dx                  NUMBER(2),
cs_mets_eval                NUMBER(1),
CS_REG_LN_EVAL              NUMBER(1),
CS_SITE_SPEC_F1             VARCHAR2(3),
cs_site_spec_f2             VARCHAR2(3),
CS_SITE_SPEC_F3             VARCHAR2(3),
cs_site_spec_f4             VARCHAR2(3),
cs_site_spec_f5             VARCHAR2(3),
CS_SITE_SPEC_F6             VARCHAR2(3),
cs_tum_size                 VARCHAR2(3),
derived_ajcc_flag           NUMBER(1),
derived_ajcc_m              NUMBER(2),
DERIVED_AJCC_M_DESC         VARCHAR2(1),
derived_ajcc_n              NUMBER(2),
derived_ajcc_n_desc         VARCHAR2(1),
derived_ajcc_stg_grp        NUMBER(2),
derived_ajcc_t              NUMBER(2),
derived_ajcc_t_desc         VARCHAR2(1),
dateofmultipletumors        NUMBER(8),
dateofconclusivedx          NUMBER(8),
MULTTUMRPTASONEPRIM   NUMBER(8),
dateconclusivedxflag        NUMBER(2),
GRADEPATHSYSTEM             NUMBER(1),
gradepathvalue              NUMBER(1),
lymphvascularinvasion       NUMBER(1),
cssitespecificfactor7       VARCHAR2(3),
CSSITESPECIFICFACTOR8       VARCHAR2(3),
cssitespecificfactor9       VARCHAR2(3),
cssitespecificfactor10      VARCHAR2(3),
cssitespecificfactor11      VARCHAR2(3),
CSSITESPECIFICFACTOR12      VARCHAR2(3),
cssitespecificfactor13      VARCHAR2(3),
cssitespecificfactor14      VARCHAR2(3),
cssitespecificfactor15      VARCHAR2(3),
cssitespecificfactor16      NUMBER(3),
cssitespecificfactor17      NUMBER(3),
CSSITESPECIFICFACTOR18      NUMBER(3),
cssitespecificfactor19      VARCHAR2(3),
cssitespecificfactor20      VARCHAR2(3),
cssitespecificfactor21      VARCHAR2(3),
cssitespecificfactor22      NUMBER(3),
CSSITESPECIFICFACTOR23      NUMBER(3),
cssitespecificfactor24      NUMBER(3),
cssitespecificfactor25      VARCHAR2(3),
DERIVEDAJCC7M               NUMBER(3),
DERIVEDAJCC7MDESCRIPT       VARCHAR2(1),
derivedajcc7n               NUMBER(3),
DERIVEDAJCC7NDESCRIPT       VARCHAR2(1),
derivedajcc7stagegrp        VARCHAR2(3),
DERIVEDAJCC7T               VARCHAR2(3),
derivedajcc7tdescript       VARCHAR2(1),
tobaccousenos               NUMBER(1),
tnm_m_code_clinical         VARCHAR2(5),
tnm_m_code_path             VARCHAR2(5),
tnm_n_code_clinical         VARCHAR2(5),
tnm_n_code_path             VARCHAR2(5),
tnm_stage_clinical          VARCHAR2(5),
tnm_stage_path              VARCHAR2(5),
tnm_t_code_clinical         VARCHAR2(5),
tnm_t_code_path             VARCHAR2(5),
behavior                    NUMBER(1),
seerwho VARCHAR2(20),
RACEGROUP                   NUMBER(1),
sumstage                    NUMBER(1),
stage_seer                      NUMBER(2),
medvalue                    NUMBER(7),
yostscl NUMBER(10,9),
quinyost NUMBER(10,9),
yangscl              NUMBER(10,9),
YANGSCLIMPUTED              NUMBER(10,9),
quinyang                    NUMBER(1),
quinyangimputed             NUMBER(1),
yangreliability             NUMBER(1),
MDINCCTACS2011              NUMBER(6),
medrentctacs2011            NUMBER(4),
collegectacs2011            NUMBER(2,2),
NOJOBCTACS2011              NUMBER(3,2),
bluecolrctacs2011           NUMBER(2,2),
below200ctacs2011           NUMBER(3,1),
chrlson  VARCHAR2(20),
stanford_anon_id  number
);

--use the stanford_anon_id to match back to unique_id, but then discard.
--use shc_pat_id going forward

/* how the stanford_anon_id was generated --
SELECT CASE WHEN m.pat_deid IS NOT NULL THEN m.pat_deid
         ELSE prostate_deid_pat_seq.nextval END AS unique_id, */


SELECT count(1) FROM ts_prostate_ccr_data_2018;
--17157 submitted
--10169 returned, 3172 patients with ccr data who we didn't have before

--insert 132 new CCR patients not already in the mapping table
INSERT INTO ts_prostate_deid_map_patient
  SELECT prostate_deid_pat_seq.nextval AS pat_deid,
  --  distinct emr.last_name, emr.first_name, p.last_name as pm_last_name, p.first_name as pm_first_name,
    p.patient_entity_id, p.shc_pat_id, p.mrn,
    CASE WHEN ora_hash(p.mrn,90,45) = 0 THEN ora_hash(p.mrn,90,45) + trunc(dbms_random.VALUE(1,89))
         WHEN mod(p.mrn,2) = 0 THEN ora_hash(p.mrn,90,45)
         WHEN mod(p.mrn,2) = 1 THEN 0 - ora_hash(p.mrn,90,45)
         ELSE NULL END AS date_offset,
    peid_repeat_count, --indicates how many patients are linked to the same patient_entity_id. One might include only counts with 1 repeat to avoid confusion.
    p.birth_date, SYSDATE AS load_date, p.pat_map_id
  FROM ts_prostate_ccr_data_2018 ccr JOIN ts_prostate_cpic_data_042018 emr
         ON ccr.stanford_anon_id = emr.unique_id
    JOIN tris_rim.pat_map p ON p.shc_pat_id = emr.shc_pat_id
  WHERE p.mrn NOT IN (SELECT mrn FROM ts_prostate_deid_map_patient WHERE mrn IS NOT NULL)
    AND p.gender IN ('Male','Unknown')
    AND (p.death_date IS NULL OR p.death_date >= to_date('01-01-2005','MM-DD-YYYY'))
    AND p.contact_yn = 'Y'
    AND NOT REGEXP_LIKE(p.mrn,'[A-Za-z]+')  --MRNs should only contain numbers
;

COMMIT;

select * from ts_prostate_ccr_data_2018;

drop table ts_prostate_ccr_2018_final;
create table ts_prostate_ccr_2018_final as
SELECT map.pat_deid, map.mrn,
  ccr.tumor_id, ccr.site_02, ccr.LATERAL, ccr.histo_t3,
  ccr.histo_m3, ccr.differn2, ccr.seqnocen, ccr.county,
  ccr.censustrcertainty2010, ccr.chemosum, ccr.dxconf,
  ccr.dx_stg_sum, ccr.hormsum, ccr.immusum, ccr.marstat,
  ccr.xnodetu, ccr.pnodetu, ccr.radbstmod, ccr.radseq,
  ccr.radsum, ccr.norad, ccr.nosurg, ccr.typerep,
  ccr.rxsummsystemicsurseq, ccr.nnodes, ccr.surgprim,
  ccr.tustat, ccr.yeardx, ccr.payer, ccr.payer2,
  ccr.surgp1, ccr.surgp2, ccr.surgp3,
  ccr.mcode_p, ccr.ncode_p, ccr.tcode_p,
  ccr.ajcc_p,
  ccr.spanish,
  CASE
        WHEN ccr.datedx = 0 THEN NULL
        WHEN ccr.datedx <= 9999 THEN TO_DATE(ccr.datedx * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.datedx <= 999999 THEN LAST_DAY(TO_DATE(ccr.datedx * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.datedx, 'yyyymmdd')
    END AS datedx,
  CASE
        WHEN ccr.datestat = 0 THEN NULL
        WHEN ccr.datestat <= 9999 THEN TO_DATE(ccr.datestat * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.datestat <= 999999 THEN LAST_DAY(TO_DATE(ccr.datestat * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.datestat, 'yyyymmdd')
    END AS datestat,
  CASE
        WHEN ccr.rxdatec = 0 THEN NULL
        WHEN ccr.rxdatec <= 9999 THEN TO_DATE(ccr.rxdatec * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.rxdatec <= 999999 THEN LAST_DAY(TO_DATE(ccr.rxdatec * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.rxdatec, 'yyyymmdd')
    END AS rxdatec,
  CASE
        WHEN ccr.rxdateh = 0 THEN NULL
        WHEN ccr.rxdateh <= 9999 THEN TO_DATE(ccr.rxdateh * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.rxdateh <= 999999 THEN LAST_DAY(TO_DATE(ccr.rxdateh * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.rxdateh, 'yyyymmdd')
    END AS rxdateh,
  CASE
        WHEN ccr.rxdatei = 0 THEN NULL
        WHEN ccr.rxdatei <= 9999 THEN TO_DATE(ccr.rxdatei * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.rxdatei <= 999999 THEN LAST_DAY(TO_DATE(ccr.rxdatei * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.rxdatei, 'yyyymmdd')
    END AS rxdatei,
  CASE
        WHEN ccr.rxdater = 0 THEN NULL
        WHEN ccr.rxdater <= 9999 THEN TO_DATE(ccr.rxdater * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.rxdater <= 999999 THEN LAST_DAY(TO_DATE(ccr.rxdater * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.rxdater, 'yyyymmdd')
    END AS rxdater,
  CASE
        WHEN ccr.rxdatesn = 0 THEN NULL
        WHEN ccr.rxdatesn <= 9999 THEN TO_DATE(ccr.rxdatesn * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.rxdatesn <= 999999 THEN LAST_DAY(TO_DATE(ccr.rxdatesn * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.rxdatesn, 'yyyymmdd')
    END AS rxdatesn,
  CASE
        WHEN ccr.surgdate = 0 THEN NULL
        WHEN ccr.surgdate <= 9999 THEN TO_DATE(ccr.surgdate * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.surgdate <= 999999 THEN LAST_DAY(TO_DATE(ccr.surgdate * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.surgdate, 'yyyymmdd')
    END AS surgdate,
  CASE
        WHEN ccr.dtdefsurg = 0 THEN NULL
        WHEN ccr.dtdefsurg <= 9999 THEN TO_DATE(ccr.dtdefsurg * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.dtdefsurg <= 999999 THEN LAST_DAY(TO_DATE(ccr.dtdefsurg * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.dtdefsurg, 'yyyymmdd')
    END AS dtdefsurg,
  CASE
        WHEN ccr.rxdate = 0 THEN NULL
        WHEN ccr.rxdate <= 9999 THEN TO_DATE(ccr.rxdate * 10000 + 1231, 'yyyymmdd')
        WHEN ccr.rxdate <= 999999 THEN LAST_DAY(TO_DATE(ccr.rxdate * 100 + 01, 'yyyymmdd'))
        ELSE to_date(ccr.rxdate, 'yyyymmdd')
    END AS rxdate,
  ccr.age, ccr.cs_ext, ccr.cs_ln, ccr.cs_mets_dx, ccr.cs_mets_eval,
  ccr.cs_reg_ln_eval, ccr.cs_site_spec_f1, ccr.cs_site_spec_f2,
  ccr.cs_site_spec_f3, ccr.cs_site_spec_f4, ccr.cs_site_spec_f5,
  ccr.cs_site_spec_f6, ccr.cs_tum_size, ccr.derived_ajcc_flag,
  ccr.derived_ajcc_m, ccr.derived_ajcc_m_desc,
  ccr.derived_ajcc_n, ccr.derived_ajcc_n_desc,
  ccr.derived_ajcc_stg_grp, ccr.derived_ajcc_t, ccr.derived_ajcc_t_desc,
  CASE
      WHEN ccr.dateofmultipletumors = 0 THEN NULL
      WHEN ccr.dateofmultipletumors <= 9999 THEN TO_DATE(ccr.dateofmultipletumors * 10000 + 1231, 'yyyymmdd')
      WHEN ccr.dateofmultipletumors <= 999999 THEN LAST_DAY(TO_DATE(ccr.dateofmultipletumors * 100 + 01, 'yyyymmdd'))
      ELSE to_date(ccr.dateofmultipletumors, 'yyyymmdd')
   END AS dateofmultipletumors,
   CASE
      WHEN ccr.dateofconclusivedx = 0 THEN NULL
      WHEN ccr.dateofconclusivedx <= 9999 THEN TO_DATE(ccr.dateofconclusivedx * 10000 + 1231, 'yyyymmdd')
      WHEN ccr.dateofconclusivedx <= 999999 THEN LAST_DAY(TO_DATE(ccr.dateofconclusivedx * 100 + 01, 'yyyymmdd'))
      ELSE to_date(ccr.dateofconclusivedx, 'yyyymmdd')
   END AS dateofconclusivedx,
   ccr.multtumrptasoneprim, ccr.dateconclusivedxflag,
   ccr.gradepathsystem, ccr.gradepathvalue, ccr.lymphvascularinvasion,
   ccr.cssitespecificfactor7, ccr.cssitespecificfactor8,
   ccr.cssitespecificfactor9, ccr.cssitespecificfactor10,
   ccr.cssitespecificfactor11, ccr.cssitespecificfactor12,
   ccr.cssitespecificfactor13, ccr.cssitespecificfactor14,
   ccr.cssitespecificfactor15, ccr.cssitespecificfactor16,
   ccr.cssitespecificfactor17, ccr.cssitespecificfactor18,
   ccr.cssitespecificfactor19, ccr.cssitespecificfactor20,
   ccr.cssitespecificfactor21, ccr.cssitespecificfactor22,
   ccr.cssitespecificfactor23, ccr.cssitespecificfactor24,
   ccr.cssitespecificfactor25,
   ccr.derivedajcc7m, ccr.derivedajcc7mdescript,
   ccr.derivedajcc7n, ccr.derivedajcc7ndescript,
   ccr.derivedajcc7stagegrp,
   ccr.derivedajcc7t, ccr.derivedajcc7tdescript,
   ccr.tobaccousenos,
   ccr.tnm_m_code_clinical,
   ccr.tnm_m_code_path,
   ccr.tnm_n_code_clinical,
   ccr.tnm_n_code_path,
   ccr.tnm_t_code_clinical,
   ccr.tnm_t_code_path,
   ccr.tnm_stage_clinical,
   ccr.tnm_stage_path,
   ccr.behavior, ccr.seerwho,
   ccr.racegroup, ccr.sumstage, ccr.stage_seer,
   ccr.medvalue, ccr.yostscl, ccr.quinyost, ccr.yangscl,
   ccr.yangsclimputed, ccr.quinyang, ccr.quinyangimputed,
   ccr.yangreliability, ccr.mdincctacs2011, ccr.medrentctacs2011,
   ccr.collegectacs2011, ccr.nojobctacs2011, ccr.bluecolrctacs2011,
   ccr.below200ctacs2011, ccr.chrlson, sysdate as load_date
FROM ts_prostate_ccr_data_2018 ccr JOIN ts_prostate_cpic_data_042018 emr
       ON ccr.stanford_anon_id = emr.unique_id
  JOIN ts_prostate_deid_map_patient map ON map.shc_pat_id = emr.shc_pat_id
WHERE site_02 = 'C619'
;

GRANT SELECT ON ts_prostate_ccr_2018_final TO prostate_cancer;

--Step5 will copy the table over to prostate db.

SELECT o.mrn_full, o.last_name, o.first_name, o.birth_date, o.datedx,
  n.datedx as newdatedx, n.stanford_anon_id
FROM
ts_prostate_ccr_data_2018 n JOIN ts_prostate_ccr_data_2016 o ON  n.tumor_id = o.tumor_id
and n.datedx = o.datedx;
